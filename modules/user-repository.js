'use strict';

class UserRepository {

  constructor () {
    this.users = {};
  }

  generateUser (username) {
    const token = Math.random().toString(36);
    return {
      username: username,
      credit: 0,
      token: token
    };
  }

  store (user) {
    this.users[user.token] = user;
  }

  findByToken (token) {
    return this.users[token];
  }

  removeByToken (token) {
    delete this.users[token];
  }

}

module.exports = UserRepository;
