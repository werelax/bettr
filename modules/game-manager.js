'use strict';

class GameManager {
  constructor (playerPool, matchManager, gameEvents, gameCommands) {
    this.playerPool = playerPool;
    this.matchManager = matchManager;
    this.gameEvents = gameEvents;
    this.gameCommands = gameCommands;
  }

  registerPlayer (player) {
    this.gameEvents.onFindMatch(player,
                                this.findMatch.bind(this, player));
  }

  deregisterPlayer (player) {
    this.playerPool.removePlayers([player]);
  }

  findMatch (player) {
    this.playerPool.pushPlayer(player);
    this.maybeStartMatch();
  }

  maybeStartMatch () {
    if (!this.playerPool.isPairAvailable()) return;
    console.log('found!')
    const pair = this.playerPool.getPair();
    this.matchManager.createMatch(pair, this.onMatchEnd.bind(this));
  }

  onMatchEnd (matchResult) {
    const { result, winner, loser } = matchResult;
    this.gameCommands.endMatchWinner(winner, matchResult);
    this.gameCommands.endMatchLoser(loser, matchResult);
    switch (result) {
    case 'resolved':
      this.handleVictory(winner, matchResult);
      this.handleDefeat(loser, matchResult);
      this.resetPlayers(winner, loser);
      break;
    case 'abandoned':
      this.resetPlayers(winner, loser);
      break;
    default:
      console.error('** Unknown match result!', matchResult);
      this.resetPlayers(winner, loser);
      break;
    }
  }

  handleVictory (winner, matchResult) {
    // this.cashManager.handleVictory(winner);
    // this.gameCommands.updateCash(winner.cash);
  }

  handleDefeat (loser, matchResult) {
    // this.cashManager.handleDefeat(loser);
    // this.gameCommands.updateCash(loser.cash);
  }

  handleDraw (player, matchResult) {
    // ???
  }

  resetPlayers (winner, loser) {
    this.gameCommands.resetPlayer(winner);
    this.gameCommands.resetPlayer(loser);
  }
}

module.exports = GameManager;
