'use strict';

const FIND_MATCH = 'FIND_MATCH';
const PLAYER_DISCONNECTED = 'disconnect';
const DEFEAT = 'DEFEAT';

class GameEvents {

  onFindMatch ({socket}, cb) {
    socket.on(FIND_MATCH, cb);
  }

  onPlayerDisconnected ({socket}, cb) {
    socket.once(PLAYER_DISCONNECTED, cb);
  }

  onDefeat ({socket}, cb) {
    socket.once(DEFEAT, cb)
  }

}

module.exports = GameEvents;
