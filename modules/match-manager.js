'use strict';

const _ = require('lodash');

class Match {
  constructor (player1, player2, stream, onMatchEnd) {
    this.players = [player1, player2];
    this.winner = undefined;
    this.loser = undefined;
    this.onMatchEnd = onMatchEnd;
    this.interrupted = false;
    this.matchEnded = false;
    this.stream = stream;
  }

  playerDefeated (player) {
    this.matchEnded = true;
    this.loser = player;
    this.winner = _.head(_.difference(this.players, [player]));
  }

  interruptMatch (interruptor) {
    this.playerDefeated(interruptor);
    this.interrupted = true;
  }

  isMatchEnded () {
    return this.matchEnded;
  }

  getPlayers () {
    return this.players;
  }

  getStream () {
    return this.stream;
  }

  getWinner () {
    return this.winner;
  }

  getLoser () {
    return this.loser;
  }

  getResultState () {
    if (this.interrupted) {
      return 'interrupted';
    } else {
      return 'resolved';
    }
  }

  getResult () {
    return {
      winner: this.getWinner(),
      loser: this.getLoser(),
      result: this.getResultState()
    };
  }
}

class MatchManager {
  constructor (gameEvents, gameCommands, GameStream) {
    this.gameEvents = gameEvents;
    this.gameCommands = gameCommands;
    this.GameStream = GameStream;
    this.ongoingMatches = [];
  }

  createMatch ([player1, player2], cb) {
    const stream = new this.GameStream(player1, player2,
                                       this.gameEvents, this.gameCommands);
    const match = new Match(player1, player2, stream, cb);
    this.gameEvents.onPlayerDisconnected(
      player1, this.interruptMatch.bind(this, match, player1)
    )
    this.gameEvents.onPlayerDisconnected(
      player2, this.interruptMatch.bind(this, match, player2)
    )
    this.gameEvents.onDefeat(
      player1, this.playerDefeated.bind(this, match, player1)
    )
    this.gameEvents.onDefeat(
      player2, this.playerDefeated.bind(this, match, player2)
    )
    this.startMatch(match);
  }

  startMatch (match) {
    const [player1, player2] = match.getPlayers();
    this.gameCommands.startMatch(player1);
    this.gameCommands.startMatch(player2);
  }

  playerDefeated (match, defeatedPlayer) {
    match.playerDefeated(defeatedPlayer);
    this.resolveMatch(match);
  }

  interruptMatch (match, interrupter) {
    match.interruptMatch();
    this.resolveMatch(match);
  }

  resolveMatch (match) {
    match.getStream().stopStream();
    match.onMatchEnd(match.getResult());
  }

}

module.exports = MatchManager;
