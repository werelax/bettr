'use strict';

const START_MATCH = 'START_MATCH';
const RESET_PLAYER = 'RESET_PLAYER';
const END_MATCH = 'END_MATCH';

class GameCommands {

  startMatch (player) {
    if (!player) return;
    const socket = player.socket;
    console.log('>> command: startMatch');
    socket.emit(START_MATCH);
  }

  resetPlayer (player) {
    if (!player) return;
    const socket = player.socket;
    console.log('>> command: resetPlayer');
    socket.emit(RESET_PLAYER);
  }

  endMatchWinner (player, matchResult) {
    if (!player) return;
    const socket = player.socket;
    console.log('>> command: endMatchWinner');
    socket.emit(END_MATCH, {winner: true});
  }

  endMatchLoser (player, matchResult) {
    if (!player) return;
    const socket = player.socket;
    console.log('>> command: endMatchLoser');
    socket.emit(END_MATCH, {winner: false});
  }
}

module.exports = GameCommands;
