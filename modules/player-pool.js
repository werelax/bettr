'use strict';

const _ = require('lodash');

class PlayerPool {
  constructor () {
    this.freePlayers = [];
  }

  pushPlayer (player) {
    if (_.includes(this.freePlayers, player)) return;
    console.log('push player!');
    this.freePlayers.push(player);
  }

  removePlayers (players) {
    console.log('remove player!');
    _.pull(this.freePlayers, players)
  }

  isPairAvailable () {
    console.log('isPoolAvailable?', this.freePlayers.length >= 2);
    return this.freePlayers.length >= 2;
  }

  getPair () {
    const pair = _.take(this.freePlayers, 2);
    this.freePlayers = _.drop(this.freePlayers, 2);
    return pair;
  }

}

module.exports = PlayerPool;
