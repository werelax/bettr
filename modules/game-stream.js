'use strict';

const _ = require('lodash');

const GAME_EVENT = 'GAME_EVENT';

class GameStream {
  constructor (player1, player2, gameEvents, gameCommands) {
    this.players = [player1, player2];
    this.gameEvents = gameEvents;
    this.gameCommands = gameCommands;
    this.connect(player1, player2);
    this.connect(player2, player1);
  }

  connect (src, target) {
    src.socket.on(GAME_EVENT,
                  (data) => target.socket.emit(GAME_EVENT, data));
  }

  stopStream () {
    _.forEach(this.players,
              (p) => p.socket && p.socket.removeAllListeners(GAME_EVENT))
  }

}

module.exports = GameStream;
