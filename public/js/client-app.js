// state

var store = {
  state: {},
  update: function (newState) {
    store.state = newState;
    store.publish(newState);
  },
  listen: function (listener) {
    store._listeners.push(listener);
  },
  _listeners: [],
  publish: function (state) {
    store._listeners.forEach(function (fn) {
      fn(state);
    });
  }
};

// actions

function login () {
  
}

function getUserinfo () {
  
}

// simple component

var pages = {
  
};

