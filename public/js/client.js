var socket = io();

socket.on('START_MATCH', function () {
  console.log('found!');
  console.log('<< start match!');
});

socket.on('RESET_PLAYER', function () {
  console.log('<< reset player!');
});

socket.on('END_MATCH', function (winner) {
  console.log('<< end match! winner? ', winner);
});

socket.on('GAME_EVENT', function (data) {
  console.log('<< GAME_EVENT:', data);
});

function findMatch() {
  socket.emit('FIND_MATCH');
  console.log('searching...');
}

function loose() {
  socket.emit('DEFEAT');
}
