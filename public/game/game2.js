
  console.log('ON');
  socket.on('GAME_EVENT', function (state) {
    console.log('HOLA!', state)
  });
(function () {
  'use strict';

  var TOTAL_BGS = 22;

  var game = new Phaser.Game(800, 600, Phaser.AUTO, 'game2', { preload: preload, create: create, update: update, render: render });
  var bgNumber = game.rnd.integerInRange(1, 21);

  function preload() {
    game.load.image('bg', 'bgs/bg' + bgNumber + '.png');
    game.load.image('block', 'block.png');
    game.load.image('mushroom', 'mushroom2.png');
  }
  var player;
  var blocks;

  var PLAYER_VELOCITY = 650;
  var PLAYER_WIDTH = 64;
  var MAX_BLOCKS = 5;
  var obstacleSpeed = 500;
  var playerSpeed = PLAYER_VELOCITY;
  var all = 0;

  function create() {

    // BG
    game.add.image(0, 0, 'bg');
    // game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;


    // BLOCKS
    createBlocks();

    // PLAYER
    player = game.add.sprite((game.width / 2) - (PLAYER_WIDTH / 2), game.height - 100, 'mushroom');
    player.name = 'mushroom';
    game.physics.enable(player, Phaser.Physics.ARCADE);
    player.immovable = true;
  }

  function update() {
    player.body.position.x = state.player.position.x;
    player.body.position.y = state.player.position.y;

    state.blocks.forEach(function (block, i) {
      blocks.children[i].body.position.x = block.position.x;
      blocks.children[i].body.position.y = block.position.y - 400;
    });
  }

  function createBlocks() {
    blocks = game.add.group();
    blocks.y = -400;

    for (var i = 0; i < MAX_BLOCKS; i++) {
      blocks.create(0, 0, 'block');
    }

    game.physics.enable(blocks, Phaser.Physics.ARCADE);
  }

  function render(){}
}());
