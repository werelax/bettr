'use strict';

const express = require('express');
const cookieSession = require('cookie-session');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const cors = require('cors');

const PlayerPool = require('./modules/player-pool');
const MatchManager = require('./modules/match-manager');
const GameStream = require('./modules/game-stream');
const GameEvents = require('./modules/game-events');
const GameCommands = require('./modules/game-commands');
const GameManager = require('./modules/game-manager');
const UserRepository = require('./modules/user-repository');

// init
const pool = new PlayerPool();
const gameEvents = new GameEvents();
const gameCommands = new GameCommands();
const matchManager = new MatchManager(gameEvents, gameCommands, GameStream);
const gameManager = new GameManager(pool, matchManager, gameEvents,
                                    gameCommands);

const userRepository = new UserRepository();

io.on('connection', (socket) => {
  let player = {socket, token: null};

  socket.on('REGISTER_PLAYER', (userInfo) => {
    player = Object.assign(player, userInfo);
    console.log('registering:', userInfo);
    gameManager.registerPlayer(player);
  });

  socket.on('disconnect', () => {
    gameManager.deregisterPlayer(player);
    userRepository.removeByToken(player.token);
  });

});

// routes

app.use(cookieSession({
  name: 'session',
  keys: ['akandemorl']
}));
app.use(cors());

app.get('/', (req, res) => {
  if (req.session.user) {
    res.sendFile(__dirname + '/public/index.html');
  } else {
    res.redirect('/login');
  }
})

app.get('/login', (req, res) => {
  res.sendFile(__dirname + '/public/login.html');
})

app.post('/login', (req, res) => {
  const username = req.params.username;
  const user = userRepository.generateUser(username);
  req.session.user = user;
  userRepository.store(user);
  res.redirect('/');
});

app.get('/logout', (req, res) => {
  const user = req.session.user;
  userRepository.removeByToken(user.token);
  req.params.user = false;
  res.redirect('/login');
})

app.get('/debug-sign-me-up', (req, res) => {
  const user = userRepository.generateUser('test-user');
  userRepository.store(user);
  req.session.user = user;
  res.send(user);
});

app.get('/me', (req, res) => {
  console.log('woot');
  res.send(req.session.user);
});

app.post('/cashin', (req, res) => {
  // 1. add some money to the user
});

app.post('/cashout', (req, res) => {
  // 1. just put the money balance to 0?
});


app.use(express.static('public'));


http.listen(3001, () => console.log('listening'));
